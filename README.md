# File Compare sample
*The instructions here assume that the sample resources have been checked-out, cloned or downloaded and unzipped into the samples directory of the DocBook-Compare release. The resources should be located such that they are two levels below the top level release directory that contains the jar files.*

*For example `DeltaXML-DocBook-Compare-5_2_2_j/samples/sample-name`.*

---

## Running the sample via the Ant build script

If you have Ant installed, use the build script provided to run the DocBook Compare sample. Simply type the following command to run the comparison and produce the output file *docbook-article-revision.xml*.

	ant run
	
The command above:

1. compiles the code (the single java class src/Program.java);
2. builds a jar that references both the deltaxml-docbook.jar and its dependent third-party jars (that is the jars contained in the main distribution directory); and
3. runs the program

If you don't have Ant installed, you can still run each sample individually from the command-line.

## Running the sample from the Command line

It is possible to compare two DocBook files using the command line tool from sample directory as follows.

    java -jar ../../deltaxml-docbook.jar compare docbook-article-version-1.xml docbook-article-version-2.xml docbook-article-revision.xml indent=yes
    
The *docbook-article-revision.xml* file contains the result, which is indented due to setting the *indent* parameter to *yes*. 

Other parameters are available as discussed in the [User Guide](https://docs.deltaxml.com/docbook-compare/latest/user-guide) and summarised by the following command.

    java -jar ../../deltaxml-docbook.jar describe
   

To clean up the sample directory, run the following Ant command.

	ant clean