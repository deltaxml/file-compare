// Copyright (c) 2024 Deltaman group limited. All rights reserved.

import java.io.File;
import java.io.FileNotFoundException;

import com.deltaxml.docbook.*;

class Program
{
    /**
     * This is the main method of the Program class, which is invoked at the command line.
     *
     * @param args The array of command line arguments.
     */
    public static void main(String[] args)
    {
        try
        {
            // Get a fresh comparison object
            DocBookCompare docbookCompare = new DocBookCompare();

            // Enable the output indentation option/parameter
            docbookCompare.setIndent(IndentOutput.YES);

            // Provide variables for each of the file-based compare method arguments
            File in1= new File("docbook-article-version-1.xml");
            File in2= new File("docbook-article-version-2.xml");
            File out= new File("docbook-article-revision.xml");
 
            // Run the compare (with pre/post commentry)
            System.out.println("Comparing files '"+in1+"' and '"+in2+"'");
            docbookCompare.compare(in1, in2, out);
            System.out.println("Storing results in file '"+out+"'");
        }
        catch (FileNotFoundException e)
        {
            System.err.println(e.getMessage());
            e.printStackTrace(System.err);
        }
        catch (DeltaXMLDocBookException e)
        {
            System.err.println(e.getMessage());
            e.printStackTrace(System.err);
        }
    }
}
